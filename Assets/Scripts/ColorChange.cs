﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorChange : MonoBehaviour
{
    [Header ("ParticleSystems")]
    [SerializeField] ParticleSystem Red;
    [SerializeField] ParticleSystem Green;
    [SerializeField]ParticleSystem Blue;
    [Header ("InitColors")]
    public float R;
    public float G;
    public float B;

private void Start() {
    GetComponent<Renderer>().material.SetColor("_Color",new Color(R,G,B));
    
}
    public void ColorChangeOperation(float _R,float _G,float _B){
        Debug.Log("Change color");
        R+=_R;
        G+=_G;
        B+=_B;
        if(R<0){
            R=0;
        }
        if(G<0){
            G=0;
        }
        if(B<0){
            B=0;
        }
        if(R>1){
            R=1;
        }
        if(G>1){
            G=1;
        }
        if(B>1){
            B=1;
        }




        if(_R>0){
            Red.Play();
        }
        if(_G>0){
            Green.Play();
        }
        if(_B>0){
            Blue.Play();
        }
        if(_R<=0&&_G<=0&&_B<=0){

        }
        
        GetComponent<Renderer>().material.SetColor("_Color",new Color(R,G,B));
    }
}
