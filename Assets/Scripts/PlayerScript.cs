﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    // Start is called before the first frame update
    bool falling =false;
    public void fall(){
        falling=true;
        StartCoroutine(FallLerp());       
    }
    IEnumerator FallLerp(){
        float falseGrav=0F;
            while(falling){
                falseGrav+=0.1f;
                gameObject.transform.position-=new Vector3(0,falseGrav,0);
                if(gameObject.transform.position.y<-11){
                    falling=false;
                }
            yield return new WaitForEndOfFrame();
        }
    }
}
