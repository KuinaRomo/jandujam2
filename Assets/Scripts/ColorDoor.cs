﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorDoor : MonoBehaviour
{
    public float rR,rG,rB;
    private void Start() {
        gameObject.GetComponent<Renderer>().material.SetColor("_Color",new Color(rR,rG,rB,0.5F));
    }
    public bool CheckColor(float _R,float _G,float _B){

        if(rR==_R&&rG==_G&&rB==_B){
            Destroy(gameObject);
            return true;
        }else{
            return false;
        }
    }
}
