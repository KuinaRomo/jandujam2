﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public int totalMovements;
    public int remainingMovements;
    public int actualLevel;
    private bool subiendo;
    public bool win;
    [SerializeField] int[] movementsPerLevel;
    [SerializeField] TextMeshProUGUI movementsText;
    [SerializeField] TextMeshProUGUI totalRed;
    [SerializeField] TextMeshProUGUI totalGreen;
    [SerializeField] TextMeshProUGUI totalBlue;
    [SerializeField] ColorChange colorChange;
    [SerializeField] GameObject[] allLevels;
    [SerializeField] GameObject parent;
    public float levelR = 0;
    public float levelG = 0;
    public float levelB = 0;
    public TextMeshProUGUI Derrota;

    // Start is called before the first frame update
    void Start() {
        //Poner aqui total movimientos por nivel
        win=false;
        actualLevel = 0;
        totalMovements = movementsPerLevel[actualLevel];
        remainingMovements = totalMovements;
        movementsText.text = "Movements: " + totalMovements + " / " + remainingMovements;
        initialLevelColors(1,0,0);
        InvokeRepeating("upLevel", 0f, 0.05f);
    }

    // Update is called once per frame
    void Update() {
        movementsText.text = "Movements: " + remainingMovements + " / " + totalMovements;
        totalRed.text = "Total Red: " + colorChange.R * 100 + " %";
        totalGreen.text = "Total Green: " + colorChange.G * 100 + " %";
        totalBlue.text = "Total Blue: " + colorChange.B * 100 + " %";
    }

    public void changeMovements() {
        if(movementsPerLevel.Length - 1 != actualLevel) {
            actualLevel++;
            totalMovements = movementsPerLevel[actualLevel];
            remainingMovements = totalMovements;
        }
    }

    public void Lose() {
        Debug.Log("Derrota");
        Derrota.gameObject.SetActive(true);
        Invoke("ReturnToMenu",2F);
    }

    public void ReturnToMenu() {
        SceneManager.LoadScene("Menu");
    }

    public void Win() {
        win = true;
        switch(actualLevel){
            case 0:
            initialLevelColors(1,0,0);
            changeLevel();
            break;
            case 1:
            initialLevelColors(-1,0,1);
            changeLevel();
            break;
            case 2:
            initialLevelColors(-1,1,0);
            changeLevel();
            break;
            case 3:
            initialLevelColors(0,0.5F,0.5F);
            changeLevel();
            break;
            case 4:
            initialLevelColors(0,0.5F,0.5F);
            changeLevel();
            break;
            case 5:
            initialLevelColors(1,0,0);
            changeLevel();
            break;
        }
        Debug.Log("Win");
    }

    void upLevel() {
        Debug.Log("Subiendo nivel");
        allLevels[actualLevel].gameObject.SetActive(true);
        if (allLevels[actualLevel].transform.position.y < 0) {
            allLevels[actualLevel].transform.position = new Vector3(allLevels[actualLevel].transform.position.x,
                                                                    allLevels[actualLevel].transform.position.y + 1,
                                                                    allLevels[actualLevel].transform.position.z);
        }
        else {
            colorChange.ColorChangeOperation(0, 0, 0);
            colorChange.ColorChangeOperation(levelR, levelG, levelB);
            CancelInvoke("upLevel");
            win=false;
        }
    }

    void downLevel() {
        Debug.Log("Subiendo nivel");
        allLevels[actualLevel].gameObject.SetActive(true);
        allLevels[actualLevel].transform.position = new Vector3(allLevels[actualLevel].transform.position.x,
                                                                    allLevels[actualLevel].transform.position.y - 1,
                                                                    allLevels[actualLevel].transform.position.z);
    }

    void deleteLevel() {
        CancelInvoke("downLevel");
        Destroy(allLevels[actualLevel].gameObject);
        actualLevel++;
        InvokeRepeating("upLevel", 0f, 0.05f);
    }

    public void initialLevelColors(float R, float G, float B) {
        levelR = R;
        levelG = G;
        levelB = B;
    }

    public void changeLevel() {
        if(actualLevel != allLevels.Length - 1) {
            InvokeRepeating("downLevel", 0f, 0.05f);
            Invoke("deleteLevel", 2);
        }
    }
}
