﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class CuboPinturaController : MonoBehaviour
{
    public float bR,bG,bB;
    public TextMeshProUGUI txtporpin;
    public float velrotation;
    //public float numporpint;
    public GameObject bucket;
    [SerializeField] AudioSource AudioLevel;

    public Renderer paintMat;
    public ParticleSystem partSystem;
    private string porpint;
    
    

    // Start is called before the first frame update
    void Start()
    {
        paintMat.material.SetColor("_Color",new Color(bR,bG,bB,1));
        
        partSystem.startColor=new Color(bR,bG,bB,0.5F);
        porpint= "R:"+(bR*100)+"%  G:"+(bG*100)+"%  B:"+(bB*100)+"%";

        txtporpin.text = porpint;
        
    }

    void FixedUpdate()
    {
        bucket.transform.Rotate(0, velrotation, 0);
    }    
    private void OnTriggerEnter(Collider other) {
        
        if(other.gameObject.tag=="Player"){
            AudioLevel.Play();
            other.gameObject.GetComponent<ColorChange>().ColorChangeOperation(bR,bG,bB);
            Destroy(gameObject);
        }
    }
    
}
