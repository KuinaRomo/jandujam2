﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldBehaviour : MonoBehaviour
{
    //[SerializeField] MeshCollider OutWorld;
    [SerializeField] GameManager gm;
    private bool rotating=false;
    public GameObject buttons;
    public PlayerScript player;
    public void moveLeft(){
        if(!rotating)
        StartCoroutine(LerpMovement(90));
    }
    public void moveRight(){
        if(!rotating)
        StartCoroutine(LerpMovement(-90));
    }
    private IEnumerator LerpMovement(float movement){
        rotating=true;
        if(movement>0){
            for(float sum=0;sum<90;sum+=2F){
                gameObject.transform.Rotate(0,2F,0,Space.World);
             yield return new WaitForEndOfFrame();
             }
             buttons.transform.Rotate(0,-90,0,Space.World);
        }
        else if(movement<0){
            for(float sum=0;sum>-90;sum-=2F){
                gameObject.transform.Rotate(0,-2F,0,Space.World);
             yield return new WaitForEndOfFrame();
             }
             buttons.transform.Rotate(0,90,0,Space.World);
        }
       
        rotating=false;
        
    }

    private void OnTriggerExit(Collider other) {
        if(other.gameObject.tag=="Player"){
            if (!gm.win) {
                player.fall();
                gm.Lose();
            }
        }
    }
}

