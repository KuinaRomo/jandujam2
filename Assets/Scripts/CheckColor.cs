﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckColor : MonoBehaviour
{

    public GameObject color;
    public GameObject GameController;

    private float colorred;
    

    // Start is called before the first frame update
    void Start()
    {
        colorred = color.GetComponent<ColorChange>().R-color.GetComponent<ColorChange>().G-color.GetComponent<ColorChange>().B;
        
    }

    // Update is called once per frame
    void Update()
    {
        colorred = color.GetComponent<ColorChange>().R;
    }

    void OnTriggerEnter(Collider coll)
    {
        
        if (coll.gameObject.tag == "Player")
        {
            Debug.Log("Collision");
            if (iscolorred(colorred) == true)
            {
                GameController.GetComponent<GameManager>().Win(); 
                Destroy(gameObject);
            }
        }
        
    }

    private bool iscolorred(float colorred)
    {
        bool red;

        if(colorred >= 0.75f)
        {
            red = true;
        }
        else
        {
            red = false;
        }

        return red;
    }
}
