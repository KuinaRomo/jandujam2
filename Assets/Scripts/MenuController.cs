﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    private RaycastHit collision;
    

    // Start is called before the first frame update
    void Start() {
        
    }

    // Update is called once per frame
    void FixedUpdate()  {
         if (Input.GetKeyDown(KeyCode.Mouse0) == true)  {
            
            if(Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out collision) == true) {
                Debug.Log(collision.collider.name);
                if(collision.collider.name == "StartCube")  {
                    SceneManager.LoadScene("VanikWorld");
                }
                if(collision.collider.name == "Exit")  {
                    Debug.Log("GodBye");
                    Application.Quit(); 
                }
                if(collision.collider.name == "Credits") {
                    SceneManager.LoadScene("Credits");
                }
                if (collision.collider.name == "GoToMenu") {
                    SceneManager.LoadScene("Menu");
                }
            }
         }
    }

    public void changeScene(string sceneName) {
        SceneManager.LoadScene(sceneName);
    }

    public void exitGame() {
        Application.Quit();
    }
}
