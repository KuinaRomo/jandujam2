﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetColor : MonoBehaviour
{
    [SerializeField] GameObject Player;
    [SerializeField] GameObject Father;
    [SerializeField] AudioSource rubberAudio;
    // Start is called before the first frame update
    void Start() {
        
    }

    // Update is called once per frame
    void Update() {
        
    }

    private void OnTriggerEnter(Collider other) {
        Debug.Log("Entro");
        if (other.gameObject.tag == "Player") {
            Debug.Log("Entro");
            Player.GetComponent<Renderer>().material.SetColor("_Color", new Color(1, 1, 1));
            Player.GetComponent<ColorChange>().R=0;
            Player.GetComponent<ColorChange>().G=0;
            Player.GetComponent<ColorChange>().B=0;
            rubberAudio.Play();
            Destroy(Father.gameObject);
        }
    }
}
