﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveCube : MonoBehaviour {

    [SerializeField] GameObject Player;
    [SerializeField] GameObject Cube;
    [SerializeField] GameManager GameManager;
    [SerializeField] AudioSource moveAudio;
    [SerializeField] float moveX = 0;
    float temporalMoveX = 0;
    [SerializeField] int turnX = 0;
    int temporalTurnX = 0;
    [SerializeField] float moveZ = 0;
    float temporalMoveZ = 0;
    [SerializeField] int turnZ = 0;
    int temporalTurnZ = 0;
    [SerializeField] float timeToMove;
    private RaycastHit hit;
    private RaycastHit DoorHit;

    bool turning = false;
    static bool startTurning = false;
    int timesTurn = 10;
    float temp = 0;
    

    // Start is called before the first frame update
    void Start() {
        
    }

    // Update is called once per frame
    void FixedUpdate() {
        if(timesTurn < 10 && !turning && startTurning) {
            temp += temp + Time.deltaTime;
            if(temp > timeToMove) {
                turning = true;
            }
        }
        
        if (startTurning) {
            if (turning) {
                moveTheCube();
                timesTurn++;
                if(timesTurn == 10) {
                    startTurning = false;
                }
                turning = false;
                temp = 0;
            }
        }
    }

    private void Update() {
        
        if (Input.GetKeyDown(KeyCode.Mouse0)) {
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit) == true) {
                if(hit.collider.gameObject == this.gameObject && !startTurning) {
                    moveAudio.Play();
                    if(Physics.Raycast(Player.transform.position,transform.right,out DoorHit,1F)&&DoorHit.collider.gameObject.tag=="ColorDoor"){
                        Debug.Log(DoorHit.collider.gameObject.tag);
                        if(DoorHit.collider.gameObject.GetComponent<ColorDoor>().CheckColor(Cube.GetComponent<ColorChange>().R,Cube.GetComponent<ColorChange>().G,Cube.GetComponent<ColorChange>().B)) {
                            if (GameManager.remainingMovements > 0) {
                                GameManager.remainingMovements--;
                                if (GameManager.remainingMovements == 0) {
                                    Debug.Log("Game Over");
                                    GameManager.Lose();
                                }
                                startTurning = true;
                                timesTurn = 0;
                            }
                        }
                        else {
                            Debug.Log("No Puedes Pasar");
                        }
                    }
                    else {
                        if (GameManager.remainingMovements > 0) {
                            GameManager.remainingMovements--;
                            if (GameManager.remainingMovements == 0) {
                                Debug.Log("Game Over");
                                GameManager.Lose();
                            }
                            startTurning = true;
                            timesTurn = 0;
                        }
                    }
                }
            }
        }
    }

   private void moveTheCube() {
        temporalMoveX = moveX / 10;
        temporalMoveZ = moveZ / 10;
        temporalTurnX = turnX / 10;
        temporalTurnZ = turnZ / 10;
        Player.transform.position = new Vector3(Player.transform.position.x + temporalMoveX,
                                                Player.transform.position.y,
                                                Player.transform.position.z + temporalMoveZ);
        Cube.transform.Rotate(temporalTurnX, 0, temporalTurnZ, Space.World);
    }
}
